### Module 
* Creating a module 
`php artisan module:make <module-name>`
* Creating a modules
`php artisan module:make Blog User Auth`
* Module path function
`$path = module_path('Blog');`
### Module manager
* module:use
`php artisan module:use Blog`
* module:unuse
`php artisan module:unuse`
* List all available modules
`php artisan module:list`
* module:enable
`php artisan module:enable Blog`
* module:disable
`php artisan module:disable Blog`
### Module Database
* module:migrate
`php artisan module:migrate Blog`
* module:migrate-rollback
`php artisan module:migrate-rollback Blog`
* module:migrate-refresh
`php artisan module:migrate-refresh Blog`
* module:migrate-reset Blog
`php artisan module:migrate-reset Blog`
* module:seed
`php artisan module:seed Blog`
### Generator commands
`module:make-<name> <file_name> <module_name>`
* module:make-command 
`php artisan module:make-command CreatePostCommand Blog`
* module:make-migration
`php artisan module:make-migration create_posts_table Blog`
* module:make-seed
`php artisan module:make-seed seed_fake_blog_posts Blog`
* module:make-controller
`php artisan module:make-controller PostsController Blog`
* module:make-model
`php artisan module:make-model Post Blog`
> --fillable=field1,field2: set the fillable fields on the generated model  
> --migration, -m: create the migration file for the given model
* module:make-provider
`php artisan module:make-provider BlogServiceProvider Blog`
* module:make-middleware
`php artisan module:make-middleware CanReadPostsMiddleware Blog`
* module:make-mail
`php artisan module:make-mail SendWeeklyPostsEmail Blog`
* module:make-notification
`php artisan module:make-notification NotifyAdminOfNewComment Blog`
* module:make-listener
```
php artisan module:make-listener NotifyUsersOfANewPost Blog  
php artisan module:make-listener NotifyUsersOfANewPost Blog --event=PostWasCreated  
php artisan module:make-listener NotifyUsersOfANewPost Blog --event=PostWasCreated --queued
```
* module:make-request
`php artisan module:make-request CreatePostRequest Blog`
* module:make-event
`php artisan module:make-event BlogPostWasUpdated Blog`
* module:make-job
```
php artisan module:make-job JobName Blog
php artisan module:make-job JobName Blog --sync # A synchronous job class
```
* module:route-provider
`php artisan module:route-provider Blog`
* module:make-factory
`php artisan module:make-factory FactoryName Blog`
* module:make-policy
`php artisan module:make-policy PolicyName Blog`
* module:make-rule
`php artisan module:make-rule ValidationRule Blog`
* module:make-resource
```
php artisan module:make-resource PostResource Blog
php artisan module:make-resource PostResource Blog --collection
```
* module:make-test
`php artisan module:make-test EloquentPostRepositoryTest Blog`
