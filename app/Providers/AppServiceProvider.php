<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class AppServiceProvider extends ServiceProvider
{

    protected $dev_providers = [
        '\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider',
        'DevModule\AppManager\AppManagerServiceProvider'
    ];

    protected $app_providers = [
        'App\Module\LaravelModulesServiceProvider'
    ];

    protected $facadeAliases = [
        'Module' => 'App\Module\Facades\Module'
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServiceProviders();
        $this->registerFacadeAliases();

        if ($this->app->environment() !== 'production') {
            $this->registerDevServiceProviders();
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Load local service providers
     */
    protected function registerDevServiceProviders()
    {
        foreach ($this->dev_providers as $provider) {
            if (class_exists($provider))
                $this->app->register($provider);
        }
    }

    protected function registerServiceProviders()
    {
        foreach ($this->app_providers as $provider) {
            if (class_exists($provider))
                $this->app->register($provider);
        }
    }

    /**
     * Load additional Aliases
     */
    public function registerFacadeAliases()
    {
        $loader = AliasLoader::getInstance();
        foreach ($this->facadeAliases as $alias => $facade) {
            if (class_exists($facade))
                $loader->alias($alias, $facade);
        }
    }
}
