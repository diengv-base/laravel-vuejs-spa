<?php


namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AppController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests;

    public function index()
    {
        return view('app');
    }
}
