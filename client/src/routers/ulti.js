/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: ulti.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/routers/ulti.js
 */

export const LoadComponent = (path) => () => import(`@/${path}.vue`)

export const LoadView = (name, index = false) =>
    LoadComponent(`resources/views/${name}${index ? '/index' : ''}`)

