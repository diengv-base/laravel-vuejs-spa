/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: resize-breakpoint.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/directive/resize-breakpoint.js
 */

import ResizeObserver from 'resize-observer-polyfill'

export default {
    bind: function (el, binding, vnode) {
        const ro = new ResizeObserver(function (entries) {
            const defaultBreakpoints = {SM: 310, MD: 500, LG: 768};
            if (binding.value && _.isObject(binding.value))
                Object.assign(defaultBreakpoints, binding.value)

            entries.forEach(function (entry) {
                const breakpoints = entry.target.dataset.breakpoints ?
                    JSON.parse(entry.target.dataset.breakpoints) :
                    defaultBreakpoints;

                Object.keys(breakpoints).map(function (breakpoint) {
                    const minWidth = breakpoints[breakpoint];
                    if (entry.contentRect.width <= minWidth) {
                        entry.target.classList.add(breakpoint);
                    } else {
                        entry.target.classList.remove(breakpoint);
                    }
                });
            });
        });

        // and start observing them.
        ro.observe(el);
    },
    unbind: function (el) {

    },
}
