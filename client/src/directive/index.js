/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/directive/index.js
 */

import LongPress from './longpress'

export default (Vue) => {
    Vue.directive('longpress', LongPress)
}

