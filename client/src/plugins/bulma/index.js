/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/bulma/index.js
 */

import BulmaNav from './component/Navbar'
import ModalCard from './component/modal-card'
import Slide from './component/slider'
import NumberInput from './component/number-input'
import FieldInput from './component/field-input'
import BulmaDropdown from './component/bulma-dropdown'
import BulmaDropdownSearch from './component/bulma-dropdown-search'

export default {
  install (Vue, options) {
    Vue.component('bulma-nav', BulmaNav)
    Vue.component('bulma-modal-card', ModalCard)
    Vue.component('bulma-slide', Slide)
    Vue.component('bulma-number-input', NumberInput)
    Vue.component('bulma-field-input', FieldInput)
    Vue.component('bulma-dropdown', BulmaDropdown)
    Vue.component('bulma-dropdown-search', BulmaDropdownSearch)
  }
}
