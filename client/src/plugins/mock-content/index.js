/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/mock-content/index.js
 */

import * as MockComponents from './core/components';
import ColorSwitch from './components/ColorSwitch.vue';

export default {
    install(Vue, options) {
        Object.keys(MockComponents).forEach(c => Vue.component(c, MockComponents[c]));

        Vue.component('ColorSwitch', ColorSwitch);
    }
}
