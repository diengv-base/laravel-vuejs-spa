/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: components.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/mock-content/core/components.js
 */

import MockContentLoading from '../components/MockContent.vue';

import MockCode from '../components/presets/Code.vue';
import MockList from '../components/presets/List.vue';
import MockTwitch from '../components/presets/Twitch.vue';
import MockFacebook from '../components/presets/Facebook.vue';
import MockInstagram from '../components/presets/Instagram.vue';
import MockBulletList from '../components/presets/BulletList.vue';
import MockTable from '../components/presets/Table.vue';

export default MockContentLoading;

export {
    MockCode,
    MockList,
    MockTwitch,
    MockFacebook,
    MockInstagram,
    MockBulletList,
    MockTable,
    MockContentLoading,
};
