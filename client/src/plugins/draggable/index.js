/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/draggable/index.js
 */

import VueDraggable from './vuedraggable'

export default {
  install(Vue, options) {
    Vue.component('draggable', VueDraggable)
  }
}
