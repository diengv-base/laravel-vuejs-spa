/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/dynamic-layout/index.js
 */

import DynamicLayout from './dynamic-layout.vue';
import DynamicLayoutResizer from './dynamic-layout-resizer.vue';

export default {
    install(Vue, options) {
        Vue.component('dynamic-layout', DynamicLayout);
        Vue.component('dynamic-layout-resizer', DynamicLayoutResizer);
    }
}
