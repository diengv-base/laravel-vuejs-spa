/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/index.js
 */

// import Notification from './notification'
import Bulma from './bulma'
// import Axios from './axios'
// import Draggable from './draggable'
import ContentLoading from './mock-content'
import Dialog from './dialog'
import Bar from './bar'
// import DynamicLayout from './dynamic-layout'
import LazyImage from './lazy-image'

export default (Vue) => {
    Vue.use(Bulma)
    // Vue.use(Notification)
    // Vue.use(Axios)
    // Vue.use(Draggable)
    Vue.use(ContentLoading)
    Vue.use(Dialog)
    Vue.use(Bar)
    // Vue.use(DynamicLayout)
    Vue.use(LazyImage)
}


