/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/plugins/date-picker/index.js
 */

export default {
    install(Vue, options) {
        Vue.component('bs4-date-picker', () => import('./components/bs4-date-picker'))
    }
}
