/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: user-session.mixin.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/mixin/user-session.mixin.js
 */

import UserModel from "../modules/user/models/UserModel";

export default {
    computed: {
        USER_info() {
            return this.$store.getters['userSession/userInfo']
        },
        /**
         * @return {boolean}
         */
        USER_isRoot() {
            return this.USER_info.type === UserModel.TYPE.root
        },
        /**
         * @return {boolean}
         */
        USER_isAdmin() {
            return this.USER_info.type === UserModel.TYPE.root || this.USER_info.type === UserModel.TYPE.admin
        }
    }
}
