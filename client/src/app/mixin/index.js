/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/mixin/index.js
 */

import ErrorsMixin from './errors.mixin'
import UserSession from './user-session.mixin'

export default (Vue) => {
    Vue.mixin(ErrorsMixin)
    Vue.mixin(UserSession)
}
