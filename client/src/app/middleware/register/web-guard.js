/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: web-guard.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/middleware/register/web-guard.js
 */

import store from '../../../stores'

export default (to, from, next) => {
    if (to.path && to.path === '/auth/login') {
        if (store.getters['userSession/isLogin'])
            next(from)
        else
            next()
    } else {
        next()
    }
}
