/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/middleware/register/index.js
 */

import web from './web-guard'
import auth from './auth-guard'

export {web, auth}
