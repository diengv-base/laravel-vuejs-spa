/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: auth-guard.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/middleware/register/auth-guard.js
 */
import store from '../../../stores'

export default (to, from, next) => {
    store.dispatch('userSession/checkLogin').then(() => {
        let isLogin = store.getters['userSession/isLogin']

        if (isLogin)
            next()
        else
            next({path: _.env('API_USER_LOGIN', '/auth/login')})
    }).catch(() => {
        next({path: _.env('API_USER_LOGIN', '/auth/login')})
    })
}
