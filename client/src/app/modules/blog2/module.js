import AppModule from "../module";
import Guards from '../../middleware/guards'
import {auth, web} from '../../middleware/register'
// import Blog2 from './store/'

export default new AppModule('blog2')
    // .store([Blog2])
    .routers([
        {
            path: '/blog2',
            // component: AppModule.layout.DefaultLayout,
            component: import('./views/blog2'),
            beforeEnter: Guards([web, auth]),
            /* children: [
                {
                    path: 'manager',
                    name: 'blog2.manager',
                    component: () => import('./views/blog2-manager'),
                }
            ] */
        }
    ])
    .register()
