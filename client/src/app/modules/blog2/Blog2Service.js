import {API_URL} from "./enum"
import BaseService from "../../base/BaseService"
import webApiService from '../../base/WebApiService'

export class Blog2Service extends BaseService {
    constructor(props) {
        super(props)
    }
}

const blog2Service = new Blog2Service()

export default blog2Service
