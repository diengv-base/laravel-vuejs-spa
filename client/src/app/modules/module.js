/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: module.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/module.js
 */

import * as Layout from '../../resources/layout'
import Store from '../../stores'

/**
 * @typedef {Object} moduleConfigObj
 * @property {boolean} isActive
 * @property {string} moduleName
 * @property {string} viewFolder
 * @property {string} serviceFolder
 */

export default class AppModule {
    constructor(moduleName) {
        this.isActive = true
        this.moduleName = moduleName
        this.viewFolder = 'views'
        this.serviceFolder = 'service'
        this._routers = []
        this._store = []
    }

    store(arr) {
        this._store = arr
        if (!Array.isArray(this._store) && _.isObject(this._store))
            this._store = [this._store]

        this._store.map(store => {
            store.namespaced = true
            Store.registerModule(_.camelCase(store.name), _.omit(store, 'name'))
        })

        return this
    }

    routers(arr) {
        this._routers = arr

        return this
    }

    /**
     *
     * @param {moduleConfigObj} [config]
     * @return {AppModule}
     */
    register(config = {}) {
        Object.assign(this, config)

        return this
    }
}

AppModule.layout = Layout
