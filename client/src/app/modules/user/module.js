/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: module.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/module.js
 */

import AppModule from "../module";
import Guards from '../../middleware/guards'
import {auth, web} from '../../middleware/register'

export default new AppModule('user')
    .routers([
        {
            path: '/user',
            component: AppModule.layout.MasterLayout,
            beforeEnter: Guards([web, auth]),
            children: [
                {
                    path: 'manager',
                    name: 'user.manager',
                    component: () => import('./views/user-manager'),
                }
            ]
        }
    ])
    .register()
