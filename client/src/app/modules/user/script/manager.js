/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: manager.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/script/manager.js
 */

import UserModel from "../models/UserModel";
import UserService from "../UserService";
import Validate from "../../../base/validate/Validate"

export default {
    data: () => ({
        UserModel: UserModel,
        listUser: [],
        selectUser: new UserModel(),
        detailUser: new UserModel(),
        searchForm: {
            keyword: '',
            perPage: 50,
            current: 1,
            total: 0,
            status: '',
            type: ''
        },
        showDialog: false,
        isLoading: false,
        isSubmit: false,
    }),
    mounted() {
        this.initData()
    },
    watch: {
        uploadMaxSize: function (val) {
            this.detailUser.config.uploadMaxSize = val * 1024 * 1024
        }
    },
    computed: {
        uploadMaxSize: {
            get: function () {
                return this.detailUser.config.uploadMaxSize / (1024 * 1024)
            },
            // setter
            set: function (newValue) {
                this.detailUser.config.uploadMaxSize = newValue * 1024 * 1024
            }
        },
        nameOk() {
            return !Validate.isNull(this.detailUser.lastName) && !Validate.isNull(this.detailUser.firstName)
        },
        emailOk() {
            return !Validate.isNull(this.detailUser.email) && Validate.isEmail(this.detailUser.email)
        },
        passwordOk() {
            return !Validate.isNull(this.detailUser.password) && !Validate.isMin(this.detailUser.password, 6)
        }
    },
    methods: {
        async initData() {
            this.isLoading = true
            await UserService.allUser(this.searchForm)
                .then(response => {
                    this.isLoading = false
                    if (response.data) {
                        this.listUser = response.data.rows.map(item => new UserModel(item))
                        this.searchForm.total = response.data.count
                    }
                })
                .catch(err => {
                    // this.isLoading = false
                    this.$addNotification({
                        color: 'warning',
                        content: err.message
                    })
                })
        },
        showAddUser() {
            this.detailUser = new UserModel()
            this.showDialog = true
        },
        showEditUser(user) {
            // console.log(user)
            this.detailUser = _.clone(user)
            this.selectUser = user
            this.showDialog = true
        },
        deleteUser(user) {
            if (user._id)
                this.$openDialog(
                    this.$t('user.manager.conf_del_user_title'),
                    this.$t('user.manager.conf_del_user_content'),
                    {
                        closeTime: 30,
                        iconClass: 'icon-solid question',
                        width: '400px'
                    },
                    {
                        onOk: () => {
                            UserService.removeUser(user._id).then(res => {
                                this.$addNotification({
                                    color: 'success',
                                    content: this.$t('user.manager.del_user_success')
                                })
                            }).catch(err => {
                                this.$addNotification({
                                    color: 'warning',
                                    content: err.message
                                })
                            })
                        }
                    }
                )
        },
        reloadData() {
            this.initData()
        },
        searchData(val) {
            this.searchForm.current = val.page
            this.searchForm.perPage = val.perPage
            this.initData()
        },
        onDialogClose() {
            this.showDialog = false
            this.selectUser = null
        },
        dialogClose() {
            this.showDialog = false
        },
        submitUser() {
            if (!this.isSubmit) {
                // edit
                this.isSubmit = true
                if (this.detailUser._id) {
                    if (this.nameOk && this.emailOk) {
                        UserService.updateUser(this.detailUser._id, this.detailUser.rawValue())
                            .then(res => {
                                this.isSubmit = false
                                this.selectUser.syncProps(res.data)
                                this.dialogClose()
                            })
                            .catch(err => {
                                this.isSubmit = false
                                this.$addNotification({
                                    color: 'warning',
                                    content: err.message
                                })
                            })
                    } else {
                        setTimeout(() => {
                            this.isSubmit = false
                            this.$addNotification({
                                color: 'danger',
                                content: this.$t('user.manager.validate_error_submit')
                            })
                        }, 1000)
                    }
                } else { // insert
                    if (this.nameOk && this.emailOk && this.passwordOk) {
                        UserService.addUser(this.detailUser.rawValue())
                            .then(res => {
                                this.isSubmit = false
                                this.listUser.push(new UserModel(res.data))
                                this.dialogClose()
                                this.$addNotification({
                                    color: 'success',
                                    content: this.$t('user.manager.update_user_success')
                                })
                            })
                            .catch(err => {
                                this.isSubmit = false
                                this.$addNotification({
                                    color: 'warning',
                                    content: err.message
                                })
                            })
                    } else {
                        setTimeout(() => {
                            this.isSubmit = false
                            this.$addNotification({
                                color: 'danger',
                                content: this.$t('user.manager.validate_error_submit')
                            })
                        }, 1000)
                    }
                }

            }
        }
    }
}
