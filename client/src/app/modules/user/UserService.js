/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: UserService.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/UserService.js
 */

import {API_URL} from "./enum"
import BaseService from "../../base/BaseService"
import webApiService from '../../base/WebApiService'

export class UserService extends BaseService {
    constructor(props) {
        super(props)
    }

    userInfo(user_id) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.get(this.makeUrl(API_URL["customer.user.info"], {
            ser_id: user_id
        }))
    }

    /**
     * add new user
     * @param user_data instance UserModel.raw
     * @returns {AxiosPromise<UserModel>|Promise<AxiosResponse<UserModel>>}
     */
    addUser(user_data) {
        if (!this.selectCustomer) return Promise.reject('No customer')

        return webApiService.post(this.makeUrl(API_URL["customer.user.store"], {
            customer_id: this.selectCustomer
        }), _.omit(user_data, 'customer'))
    }

    blockUser(user_id) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.post(this.makeUrl(API_URL["customer.user.block"], {
            user_id: user_id
        }))
    }

    removeUser(user_id) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.delete(this.makeUrl(API_URL["customer.user.delete"], {
            user_id: user_id
        }))
    }

    /**
     * update user data
     * @param user_id UserModel._id
     * @param user_data
     * @returns {AxiosPromise<UserModel>|Promise<AxiosResponse<UserModel>>}
     */
    updateUser(user_id, user_data) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.put(this.makeUrl(API_URL["customer.user.update"], {
            user_id: user_id
        }), user_data)
    }

    updateConfig(user_id, config) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.put(this.makeUrl(API_URL["customer.user.update-config"], {
            user_id: user_id
        }), config)
    }

    /**
     * get all user by customer
     * @param config <{page: number, limit: number, keyword?: string, status?: string, type?: string}>
     * @returns {AxiosPromise<{UserModel[]>|Promise<AxiosResponse<UserModel[]>>}
     */
    allUser(config) {
        if (!this.selectCustomer) return Promise.reject({message: 'No customer'})

        const defaultConfig = {
            current: 1,
            perPage: 50
        }

        return webApiService.get(this.makeUrl(API_URL["customer.user.all"], {
            customer_id: this.selectCustomer
        }), {params: {...defaultConfig, ...config}})
    }
}

const userService = new UserService()

export default userService
