/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: UserModel.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/models/UserModel.js
 */

import BaseModel from "../../../base/BaseModel";
import UserProfileModel from "./UserProfileModel";

class UserConfig extends BaseModel {
    constructor(props) {
        super(props);
        this.uploadMaxSize = 1048576 * 20 // 20MB
        this.uploadMaxFile = 6
        this.uploadAccept = ".png,.jpg,.jpeg,.gif,.svg,.csv,.pdf,.xps,.zip,.rar,.txt,.doc,.docx,.xls,.xlsx,.ppt,.pptx"
        this.fileViewModel = 'th'

        this.syncProps(props)
    }

}

export default class UserModel extends BaseModel {
    constructor(props) {
        super(props)
        this._id = null
        this.lastName = ''
        this.firstName = ''
        this.email = ''
        this.password = ''
        this.customer = null
        this.birthday = ''
        this.profile = []
        this.status = UserModel.STATUS.inActive
        this.type = UserModel.TYPE.user
        this.config = new UserConfig()

        this.syncProps(props)
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }

    syncProps(props) {
        if (props && _.isObject(props) && ('profile' in props) && props.profile.length) {
            props.profile.map(item => new UserProfileModel(item))
        }

        return super.syncProps(props);
    }

    rawValue() {
        const val = super.rawValue();
        if (val.customer)
            val.customer = val.customer._id

        return val
    }
}

UserModel.STATUS = {
    active: 'ACTIVE',
    inActive: 'INACTIVE',
    block: 'BLOCK'
}

UserModel.TYPE = {
    root: 'ROOT',
    admin: 'ADMIN',
    customer: 'CUSTOMER',
    user: 'USER',
}
