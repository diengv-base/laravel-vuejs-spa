/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: UserProfileModel.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/models/UserProfileModel.js
 */

import BaseModel from "../../../base/BaseModel";

export default class UserProfileModel extends BaseModel {
    constructor(props) {
        super(props)
        // this._id = null
        // this.use_id = null
        this.keyName = ''
        this.keyValue = ''
        this.keyType = UserProfileModel.KEY_TYPE.text

        this.syncProps(props)
    }
}

UserProfileModel.KEY_TYPE = {
    text: 'Text',
    textArea: 'Long text',
    html: 'Html',
    image: 'Image'
}
