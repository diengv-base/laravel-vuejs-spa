/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: enum.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/user/enum.js
 */

export const API_URL = {
    'customer.user.all': '/customer/:customer_id/user/all',
    'customer.user.store': '/customer/:customer_id/user/store',
    'customer.user.update': '/customer/user/:user_id/update',
    'customer.user.update-config': '/customer/user/:user_id/update-config',
    'customer.user.info': '/customer/user/:user_id/info',
    'customer.user.delete': '/customer/user/:user_id/delete',
    'customer.user.block': '/customer/user/block/:user_id',
}
