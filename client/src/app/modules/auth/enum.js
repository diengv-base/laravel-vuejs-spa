/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: enum.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/auth/enum.js
 */

export const AUTH_URL = {
    login: _.env('API_USER_LOGIN', '/auth/login'),
    register: _.env('API_USER_REGISTER', '/auth/register'),
    logout: _.env('API_USER_LOGOUT', '/auth/logout'),
    me: _.env('API_USER_INFO', '/auth/me'),
}

export const MUTATION_TYPE = {
    updateUserInfo: 'UPDATE_USER_INFO',
    updateLoginStatus: 'UPDATE_LOGIN_STATUS',
    updateToken: 'UPDATE_TOKEN',
    updateUserConfig: 'UPDATE_USER_CONFIG'
}
