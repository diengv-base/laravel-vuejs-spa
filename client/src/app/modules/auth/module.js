/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: module.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/modules/auth/module.js
 */

import AppModule from '../module'
import UserSession from './store/user-session'
import Guards from '../../middleware/guards'
import {web} from '../../middleware/register'

export default new AppModule('auth')
    .store([UserSession])
    .routers([
        {
            path: '/auth',
            component: AppModule.layout.DefaultLayout,
            beforeEnter: Guards([web]),
            children: [
                {
                    path: 'login',
                    name: 'auth.login',
                    component: () => import('./views/login'),
                    meta: {bodyClass: 'body-login'}
                },
                {
                    path: 'register',
                    name: 'auth.register',
                    component: () => import('./views/register'),
                    meta: {bodyClass: 'body-register'}
                }
            ]
        }
    ])
    .register()
