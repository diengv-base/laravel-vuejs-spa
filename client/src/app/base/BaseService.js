/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: BaseService.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/base/BaseService.js
 */
import Store from '../../stores'

export default class BaseService {
    constructor(props) {
        this.selectCustomer = (props && props.customer) ? props.customer : Store.getters['customer/current']
        this.appCustomer = process.env.VUE_APP_CUSTOMER_ID
    }

    setCustomer(customer) {
        let tmp = ''
        if (customer && _.isObject(customer) && customer.value)
            tmp = customer.value
        else if (customer)
            tmp = customer

        this.selectCustomer = tmp

        Store.dispatch('customer/setCurrent', tmp)
    }

    makeUrl(url, params) {
        if (params) {
            Object.keys(params).map(param => {
                url = url.replace(':' + param, params[param])
            })
        }

        return url.replace(/:\w+/, '')
    }
}
