/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: WebApiService.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/base/WebApiService.js
 */

import axios from 'axios'

const webApi = axios.create({
    baseURL: window.location.origin,
    timeout: 30000,
    // withCredentials: true
    /*    httpAgent: new http.Agent({keepAlive: true}),
		httpsAgent: new https.Agent({keepAlive: true})*/
})

webApi.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    webApi.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}

webApi.setAcceptLanguage = (lang) => {
    webApi.defaults.headers.common['Accept-Language'] = lang
}

webApi.interceptors.request.use(function (config) {
    // hook
    if (!navigator.onLine)
        return Promise.reject({message: 'You are offline'})

    return config
}, function (error) {
    // hook

    return Promise.reject(error)
})

// Add a response interceptor
webApi.interceptors.response.use(function (response) {
    // hook

    return response
}, function (error) {
    // hook
    if (error.response) {
        if (error.response.data && _.isObject(error.response.data)) {
            if (error.response.data.err_code) {
                switch (error.response.data.err_code) {
                    case 'TOKEN_NO_ACCESS':
                        location.replace('/login')
                        break
                }
            }
            if (error.response.data.message)
                error.message = error.response.data.message
        } else if (error.response.data && typeof error.response.data === 'string') {
            error.message = error.response.data
        }

    }

    return Promise.reject(error)
})

export default webApi

