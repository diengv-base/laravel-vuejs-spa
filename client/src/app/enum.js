/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: enum.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/app/enum.js
 */

export const DATA_PAGINATE = [30, 50, 80, 100, 150, 200, 250, 400, 500, 1000]
