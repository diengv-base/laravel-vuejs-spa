/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: mutations-type.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/stores/mutations-type.js
 */

export default {
    updateOnlineFlag: 'UPDATE_ONLINE_FLAG',
    updatePrevRouter: 'UPDATE_PREV_ROUTER'
}
