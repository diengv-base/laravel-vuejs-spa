/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: convert.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/helpers/mixins/convert.js
 */
export default {
    register(h) {
        const bytesToSize = (bytes, str = true) => {
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes === 0) return 'n/a';
            const i = parseInt(Math.floor(Math.log(Math.abs(bytes)) / Math.log(1024)), 10);
            if (i === 0) return `${bytes} ${sizes[i]})`;
            if (str)
                return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`;

            return [`${(bytes / (1024 ** i)).toFixed(1)}`, sizes[i]];
        }

        const sizeToBytes = (sizeStr) => {
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
            const sizeInt = parseInt(sizeStr, 10)
            if (!sizeInt) return 'n/a'
            let tmp = 0
            sizes.forEach((str, index) => {
                if (sizes.indexOf(str) !== -1) {
                    if (index === 0) tmp = sizeInt
                    tmp = (1024 ** index) * sizeInt
                }
            })

            return tmp
        }

        return {bytesToSize, sizeToBytes}
    }
}
