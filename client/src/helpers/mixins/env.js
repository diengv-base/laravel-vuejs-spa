/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: env.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/helpers/mixins/env.js
 */

import * as config from '../../app-config.json'

export default {
    register(h) {
        const env = (keyName, default_value = null) => {
            const value = config[h.toUpper(h.snakeCase(keyName))]
            return value ? value : default_value
        }

        return {env: env}
    }
}
