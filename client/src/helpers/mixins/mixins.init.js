/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: mixins.init.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/helpers/mixins/mixins.init.js
 */

import uuid from './uuid'
import env from './env'
import convert from './convert'

export default {uuid, env, convert}

