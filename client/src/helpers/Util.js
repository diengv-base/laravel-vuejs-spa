/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: Util.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/helpers/Util.js
 */


export default class Util {
    static hasErrorPage(error) {
        if (error.response && error.response.status) {
            error = error.response.status
        }

        return [400, 401, 500].indexOf(error) !== -1
    }

    static camelCaseToDash(str) {
        return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase()
    }

    static snakeCaseToCamelCase(str) {
        if (typeof str !== 'string') {
            console.warn(`the ${str} is not String`)
        }

        return str.replace(/_(\w)/g, (m => m[1].toUpperCase()))
    }

    static isObject(value) {
        const type = typeof value
        return value != null && (type === 'object' || type === 'function');
    }


    static replaceUrlParam(url, params) {
        if (params && _.isObject(params)) {
            Object.keys(params).map(key => {
                url = url.replace(`:${key}`, params[key])
            })
        }

        return url
    }
}


export const stringToBytes = string => [...string].map(character => character.charCodeAt(0));

export const uint8ArrayUtf8ByteString = (array, start, end) => {
    return String.fromCharCode(...array.slice(start, end));
};

export const readUInt64LE = (buffer, offset = 0) => {
    let n = buffer[offset];
    let mul = 1;
    let i = 0;

    while (++i < 8) {
        mul *= 0x100;
        n += buffer[offset + i] * mul;
    }

    return n;
};

export const tarHeaderChecksumMatches = buffer => { // Does not check if checksum field characters are valid
    if (buffer.length < 512) { // `tar` header size, cannot compute checksum without it
        return false;
    }

    const MASK_8TH_BIT = 0x80;

    let sum = 256; // Intitalize sum, with 256 as sum of 8 spaces in checksum field
    let signedBitSum = 0; // Initialize signed bit sum

    for (let i = 0; i < 148; i++) {
        const byte = buffer[i];
        sum += byte;
        signedBitSum += byte & MASK_8TH_BIT; // Add signed bit to signed bit sum
    }

    // Skip checksum field

    for (let i = 156; i < 512; i++) {
        const byte = buffer[i];
        sum += byte;
        signedBitSum += byte & MASK_8TH_BIT; // Add signed bit to signed bit sum
    }

    const readSum = parseInt(uint8ArrayUtf8ByteString(buffer, 148, 154), 8); // Read sum in header

    // Some implementations compute checksum incorrectly using signed bytes
    return (
        // Checksum in header equals the sum we calculated
        readSum === sum ||

        // Checksum in header equals sum we calculated plus signed-to-unsigned delta
        readSum === (sum - (signedBitSum << 1))
    );
};

