/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: main.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/main.js
 */

import Vue from 'vue'
import App from './App.vue'
import router from './routers'
import store from './stores'
import './service-worker'
import Config from './config'
import {i18n} from './config/i18n-setup'
import Plugin from './plugins'
import Directive from './directive'
import AppComponent from './resources/components'
import AppMixin from './app/mixin'

// directive init
Directive(Vue)

// plugin init
Plugin(Vue)

// global app component
AppComponent(Vue)

// add global mixin
AppMixin(Vue)

// init config vendor module
Config.i18nAutoLoad(Vue, {router: router})

// App init
Vue.config.performance = process.env.NODE_ENV !== 'production'
Vue.config.productionTip = false
Vue.config.devtools=false

const RootApp = new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')

export default RootApp
