/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/resources/components/index.js
 */

export default (Vue) => {
    Vue.component('app-image', () => import('./app-image'))
    Vue.component('ajax-loader', () => import('./loaders/ajax-loader'))
    Vue.component('circles-loader', () => import('./loaders/circles-loader'))
}
