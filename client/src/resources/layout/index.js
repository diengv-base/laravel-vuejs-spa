/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: index.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/resources/layout/index.js
 */

import DefaultLayout from './default-layout'
import MasterLayout from './master-layout'



export {DefaultLayout, MasterLayout}
