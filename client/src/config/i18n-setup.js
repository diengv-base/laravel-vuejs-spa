/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: i18n-setup.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/config/i18n-setup.js
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from '../../../resources/lang/en'
import webApi from '../app/base/WebApiService'

Vue.use(VueI18n)

const findLang = () => {
    return window.navigator.language ? window.navigator.language : 'en'
}

const locale = _.env('LOCALE', findLang())

export const i18n = new VueI18n({
    locale: locale, // set locale
    fallbackLocale: 'en',
    messages: {en},
    silentTranslationWarn: true
})

const setI18nLanguage = (lang) => {
    i18n.locale = lang
    webApi.setAcceptLanguage(lang)
    document.querySelector('html').setAttribute('lang', lang)
    return lang
}

export const loadLanguageAsync = (lang) => {
    if (i18n.locale !== lang) {

        const loadedLanguages = ['en']
        if (loadedLanguages.indexOf(lang) === -1) {
            return import(
                /* webpackInclude: /\.json$/ */
                /* webpackExclude: /\.noimport\.json$/ */
                /* webpackChunkName: "lang-" */
                /* webpackMode: "lazy" */
                /* webpackPrefetch: true */
                /* webpackPreload: true */
                `../../../resources/lang/${lang}`).then(msgs => {
                i18n.setLocaleMessage(lang, msgs.default)
                loadedLanguages.push(lang)
                return setI18nLanguage(lang)
            })
        }
        return Promise.resolve(setI18nLanguage(lang))
    } else {
        setI18nLanguage(lang)
    }
    return Promise.resolve(lang)
}

loadLanguageAsync(locale)
