/*
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: app.js
 * Path: D:/projects/laravel-vuejs-spa/resources/js/config/app.js
 */

export const DATE_TIME_FORMAT = 'D/M/YYYY H:S:SS'
export const DATE_FORMAT = 'D/M/YYYY'
