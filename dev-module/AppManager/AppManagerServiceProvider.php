<?php namespace DevModule\AppManager;

use DevModule\AppManager\Providers\ApiDocGeneratorServiceProvider;
use DevModule\AppManager\Providers\QueryLogServiceProvider;
use DevModule\AppManager\Providers\HttpLoggerServiceProvider;
use Illuminate\Support\ServiceProvider;
use Artisan;

class AppManagerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/resources/assets' => public_path('vendor/dev-module/app-manager'),
        ], 'app-manager');

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'AppManager');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->app->register(HttpLoggerServiceProvider::class);
        $this->app->register(QueryLogServiceProvider::class);
        $this->app->register(ApiDocGeneratorServiceProvider::class);

        Artisan::call('vendor:publish', ['--tag' => 'app-manager', '--force' => true]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/app-manager.php', 'app-manager');
    }
}
