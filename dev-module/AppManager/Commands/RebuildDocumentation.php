<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: RebuildDocumentation.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Commands/RebuildDocumentation.php
 */

namespace DevModule\AppManager\Commands;

use Illuminate\Console\Command;
use DevModule\AppManager\Code\Documentarian;

class RebuildDocumentation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apidoc:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild your API documentation from your markdown file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return false|null
     */
    public function handle()
    {
        $outputPath = config('app-manager.output');

        $documentarian = new Documentarian();

        if (! is_dir($outputPath)) {
            $this->error('There is no existing documentation available at '.$outputPath.'.');

            return false;
        }
        $this->info('Rebuilding API HTML code from '.$outputPath.'/source/index.md');

        $documentarian->generate($outputPath);

        $this->info('Wrote HTML documentation to: '.$outputPath.'/index.html');
    }
}
