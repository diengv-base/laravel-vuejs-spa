<?php

namespace DevModule\AppManager\Middlewares;

use Closure;
use Illuminate\Http\Request;
use DevModule\AppManager\Code\LogProfile;
use DevModule\AppManager\Code\LogWriter;

class HttpLogger
{
    protected $logProfile;
    protected $logWriter;

    public function __construct(LogProfile $logProfile, LogWriter $logWriter)
    {
        $this->logProfile = $logProfile;
        $this->logWriter = $logWriter;
    }

    public function handle(Request $request, Closure $next)
    {
//        die('test');
        if ($this->logProfile->shouldLogRequest($request)) {
            $this->logWriter->logRequest($request);
        }

        return $next($request);
    }
}
