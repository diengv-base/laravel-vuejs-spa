@extends('AppManager::_layout')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/dev-module/app-manager/css/datatables.min.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('vendor/dev-module/app-manager/css/rainbow-custom.min.css')}}"/>
    <style>
        .text {
            word-break: break-all;
        }

        a.llv-active {
            z-index: 2;
            background-color: #f5f5f5;
            border-color: #777;
        }

        .list-group-item {
            word-wrap: break-word;
        }

        .folder {
            padding-top: 15px;
        }

        .div-scroll {
            height: 80vh;
            overflow: hidden auto;
        }

        .nowrap {
            white-space: nowrap;
        }

        /* .stack {
             position: absolute;
             left: 0;
             right: 0;
             width: 100%;
         }*/

    </style>
@endsection

@section('foot')
    <script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/rainbow-custom.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Rainbow.color();
            $('.table-container tr button').on('click', function () {
                $('#' + $(this).data('display')).toggle();
            });
            $('#table-log').DataTable({
                "order": [$('#table-log').data('orderingIndex'), 'desc'],
                "stateSave": true,
                "stateSaveCallback": function (settings, data) {
                    window.localStorage.setItem("datatable", JSON.stringify(data));
                },
                "stateLoadCallback": function (settings) {
                    var data = JSON.parse(window.localStorage.getItem("datatable"));
                    if (data) data.start = 0;
                    return data;
                },
                scrollX: true,
                scrollY: '70vh',
                scrollCollapse: true,
            });
            $('#delete-log, #clean-log, #delete-all-log').click(function () {
                return confirm('Are you sure?');
            });
        });

    </script>

    <script>
        if (navigator.appName == "Microsoft Internet Explorer") {
            $(window).load(function () {
                $(window).load(function () {
                    self.location.reload(true);
                });
            });
        } else {
            window.addEventListener('load', function () {
                self.name = 'refreshed';
                window.addEventListener('pageshow', function () {
                    if (self.name != 'refreshed') {
                        self.location.reload(true);
                    }
                }, false);
                window.addEventListener('pagehide', function () {
                    self.name = '';
                }, false);
                window.addEventListener('focus', function () {
                    if (self.name != 'refreshed') {
                        self.location.reload(true);
                    }
                    self.name = '';
                }, false);
                window.addEventListener('blur', function () {
                    self.name = '';
                }, false);
            }, false);
        }
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col sidebar mb-3">
            <p class="text-dark text-uppercase">Log Viewer</p>
            <div class="list-group div-scroll" style="font-size: 14px">
                @foreach($folders as $folder)
                    <div class="list-group-item list-group-flush">
                        <a href="?f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}">
                            {{$folder}}
                        </a>
                        @if ($current_folder == $folder)
                            <div class="list-group folder">
                                @foreach($folder_files as $file)
                                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}&f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}"
                                       class="list-group-item  @if ($current_file == $file) llv-active @endif">
                                        {{$file}}
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
                @foreach($files as $file)
                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
                       class="list-group-item d-flex justify-content-between align-items-center @if ($current_file == $file) active @endif">
                        {{$file}}
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-10 table-container">
            @if ($logs === null)
                <div>
                    Log file >50M, please download it.
                </div>
            @else
                <table id="table-log" class="table" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
                    <thead>
                    <tr>
                        @if ($standardFormat)
                            <th>Level</th>
                            <th>Context</th>
                            <th>Date</th>
                        @else
                            <th>Line number</th>
                        @endif
                        <th>Content</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($logs as $key => $log)
                        <tr data-display="stack{{{$key}}}">
                            @if ($standardFormat)
                                <td class="nowrap text-{{{$log['level_class']}}}">
                                    <span class="{{{$log['level_img']}}}"
                                          aria-hidden="true"></span>&nbsp;&nbsp;{{$log['level']}}
                                </td>
                                <td class="text">{{$log['context']}}</td>
                            @endif
                            <td class="date">{{{$log['date']}}}</td>
                            <td class="text">
                                @if ($log['stack'])
                                    <button type="button"
                                            class="expand btn btn-outline-dark btn-sm"
                                            data-display="stack{{{$key}}}">
                                        Show/Hide
                                    </button>
                                @endif
                                <p class="mb-0">{{{$log['text']}}}</p>
                                @if (isset($log['in_file']))
                                    <p class="mb-0">{{{$log['in_file']}}}</p>
                                @endif
                                @if ($log['stack'])
                                    <div class="stack" id="stack{{{$key}}}"
                                         style="display: none;">
                                        @if($log['has_lang']=='sql')
                                            <pre><code data-language="sql">{{{ trim($log['stack']) }}}</code></pre>
                                        @else
                                            <pre><code data-language="generic">{{{ trim($log['stack']) }}}</code></pre>
                                            @endif
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif
            <div class="p-3">
                @if($current_file)
                    <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-download"></span> Download file
                    </a>
                    -
                    <a id="clean-log"
                       href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-sync"></span> Clean file
                    </a>
                    -
                    <a id="delete-log"
                       href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-trash"></span> Delete file
                    </a>
                    @if(count($files) > 1)
                        -
                        <a id="delete-all-log"
                           href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                            <span class="fa fa-trash-alt"></span> Delete all files
                        </a>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
