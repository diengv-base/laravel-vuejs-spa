@extends('AppManager::_layout')
@section('content')
    <div class="container">
        <form method="POST" action="{{route('--dev.module.delete',['name'=>$module->getLowerName()])}}">
            @csrf
            @method('DELETE')
            <h3 class="text-danger">Delete module: {{$module->getName()}}</h3>
            <p><strong>Backend path: </strong>{{$module->getPath()}}</p>
            <p><strong>Frontend path: </strong>{{base_path('resources/js/app/modules/'.Str::kebab($module->getName()))}}
            </p>
            <p><strong>Status: </strong>
                @if($module->active)
                    <span class="badge badge-info">Enable</span>
                @else
                    <span class="badge badge-warning">Disable</span>
                @endif
            </p>
            <p class="text-danger"><strong>!!!</strong> If the module is deleted, all backend files, frontend of the
                module
                will be deleted completely <strong>!!!</strong></p>
            <button class="btn btn-danger" type="submit">Confirm</button>
            <a class="btn btn-dark text-light"
               href="{{route('--dev.module.detail',['name'=>$module->getLowerName()])}}">Cancel</a>
        </form>
    </div>
@endsection
