@extends('AppManager::_layout')
@section('content')
    <!-- Modal crete module -->
    <button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#createModuleModal">
        >:Create Module
    </button>
    <button type="button" class="btn btn-danger mb-1" id="btn-dump-autoload">
        >:dump-autoload
    </button>
    <button type="button" class="btn btn-success mb-1" id="btn-dump-generate-api-doc">
        >:generate-api-doc
    </button>
    <div class="modal fade" id="createModuleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create new module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formCreateModule" method="POST" action="{{route('--dev.module.create')}}">
                    <div class="modal-body">
                        @csrf
                        <div class="alert alert-info"></div>
                        <div class="form-group">
                            <label>Module name</label>
                            <input type="text" name="moduleName" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary modal-create-btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <thead>
        <th></th>
        <th>Name</th>
        <th>Path</th>
        <th></th>
        </thead>
        <tbody>
        @foreach($modules as $module)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>
                    {{$module->getName()}}<br/>
                    @if($module->active)
                        <span class="badge badge-info">Enable</span>
                    @else
                        <span class="badge badge-warning">Disable</span>
                    @endif
                </td>
                <td>
                    <strong>B: </strong>{{$module->getPath()}}<br/>
                    <strong>F: </strong>{{base_path('resources/js/app/modules/'.Str::kebab($module->getName()))}}
                </td>
                <td class="text-center">
                    <a href="{{route('--dev.module.detail',['name'=>$module->getLowerName()])}}"
                       class="btn btn-info">Detail</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
