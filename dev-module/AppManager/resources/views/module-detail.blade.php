@extends('AppManager::_layout')
@section('head')
<style>

</style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-10">
                <h3>Detail module: {{$module->getName()}}</h3>
                <p><strong>Providers: </strong>
                <pre><code>{{json_encode($module->providers)}}</code></pre>
                </p>
                <p><strong>Backend path: </strong>{{$module->getPath()}}</p>
                <p><strong>Frontend
                        path: </strong>{{base_path('resources/js/app/modules/'.Str::kebab($module->getName()))}}</p>
                <p><strong>Status: </strong>
                    @if($module->active)
                        <span class="badge badge-info">Enable</span>
                    @else
                        <span class="badge badge-warning">Disable</span>
                    @endif
                </p>
            </div>
            <div class="col col-2">
                @if($module->active)
                    <a class="btn btn-warning btn-block"
                       href="{{route('--dev.module.change',['action'=>'disable','name'=>$module->getLowerName()])}}">Disable</a>
                @else
                    <a class="btn btn-primary btn-block"
                       href="{{route('--dev.module.change',['action'=>'enable','name'=>$module->getLowerName()])}}">Enable</a>
                @endif
                <a class="btn btn-danger btn-block"
                   href="{{route('--dev.module.delete',['name'=>$module->getLowerName()])}}">Delete</a>
            </div>
            <div class="col col-9">
                <h3>Generator commands</h3>
                <div class="accordion" id="accordionGenerator">
                    @foreach($generators as $generator)
                        <div class="card border-info">
                            <div class="card-header p-0" id="heading-{{$generator}}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#collapse-{{$generator}}"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        module:make-{{$generator}}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-{{$generator}}" class="collapse" aria-labelledby="heading-{{$generator}}"
                                 data-parent="#accordionGenerator">
                                <div class="card-body">
                                    <form id="make_{{$generator}}_form" name="make-{{$generator}}-form" class="mb-3" method="POST" action="{{route('--dev.module.generator',['name'=>$module->getLowerName(),'generatorKey'=>$generator])}}">
                                        @csrf
                                        <div class="alert alert-info"></div>
                                        <div class="input-group">
                                           @if($generator == 'controller')
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <input type="radio" id="radio1" checked name="controller_type" value="">
                                                        <label for="radio1">ALL</label>
                                                    </div>
                                                    <div class="input-group-text">
                                                        <input type="radio" id="radio2" name="controller_type" value="api">
                                                        <label for="radio2">API</label>
                                                    </div>
                                                    <div class="input-group-text">
                                                        <input type="radio" id="radio3" name="controller_type" value="plain">
                                                        <label for="radio3">PLAIN</label>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="text" class="form-control" id="make-{{$generator}}" name="generatorName">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-dark" type="submit" onclick="actionFunction('{!! $generator !!}')">Make</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col col-3">
                <h3>Module commands</h3>
                <a class="btn btn-success btn-block">migrate</a>
                <a class="btn btn-success btn-block">migrate-rollback</a>
                <a class="btn btn-success btn-block">migrate-refresh</a>
                <a class="btn btn-success btn-block">migrate-reset</a>
                <a class="btn btn-success btn-block">seed</a>
            </div>
        </div>
    </div>
    <style>
        .input-group-prepend .input-group-text{
            padding: 0px 2px !important;
        }
        .input-group-prepend input[type="radio"] {
            display: none;
        }
        .input-group-prepend input[type="radio"]:checked+label {
            background-color: #bbb;
        }
        .input-group-prepend label {
            display: inline-block;
            background-color: #ddd;
            padding: 4px 9px;
            font-family: Arial;
            font-size: 16px;
            cursor: pointer;
            margin: 0px !important;
            border-radius: 5px;

        }

        .input-group-prepend input[type="radio"]:checked+label {
            background-color: #bbb;
        }
    </style>
@endsection
