<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>App manager</title>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/dev-module/app-manager/css/bootstrap.min.css')}}"/>
    @yield('head')
</head>
<body>
<div class="container-fluid">
    <div class="row bg-dark text-light align-items-center">
        <div class="col col-4">
            <h4 class="text-center text-uppercase">DEV APP manager<br/>
                <small>{{env('APP_NAME')}}</small>
            </h4>
        </div>
        <div class="col col-8">
            <div>
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <a class="btn btn-secondary" href="{{route('--dev.module.view')}}">Module</a>
                        <a class="btn btn-secondary" href="{{route('--dev.log.view')}}">Log view</a>
                        <a class="btn btn-secondary" href="/docs/index.html">API Doc</a>
                    </div>
                    <div class="btn-group" role="group">
                        <div class="btn-group" role="group" id="btnGroupDropClear">
                            <button id="btnGroupDropClearSl" type="button" class="btn btn-secondary dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Clear......
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDropClearSl">
                                <a class="dropdown-item" data-action="view:clear" href="#">View</a>
                                <a class="dropdown-item" data-action="route:clear" href="#">Router</a>
                                <a class="dropdown-item" data-action="config:clear" href="#">Config</a>
                                <a class="dropdown-item" data-action="session:flush" href="#">Session</a>
                                <a class="dropdown-item" data-action="cache:clear" href="#">Cache</a>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary" id="topClearBtn">Clear</button>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{$error}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endif
    <div>
        @yield('content')
    </div>

</div>
<!-- jQuery for Bootstrap -->
<script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/dev-module/app-manager/js/app-module.js')}}"></script>
@yield('foot')
</body>
</html>
