function callAjax(options = {}, callBack = {}) {
    const defaultOption = {
        url: '',
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        data: {}
    }

    const defaultCallBack = {
        beforeSend: function (jqXHR, settings) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data, textStatus, jqXHR) {
        }
    }

    const opt = Object.assign({}, defaultOption, options)
    const calb = Object.assign({}, defaultCallBack, callBack)

    return $.ajax(Object.assign({}, opt, calb))
}

document.addEventListener('DOMContentLoaded', function (event) {
    const topClearBtn = $('#topClearBtn')
    const btnGroupDropClear = $('#btnGroupDropClear')

    function cmdAction(_action) {
        const modalStatusEl = $('#modalStatus');
        const modalBody = modalStatusEl.find('.modal-body');
        const modalFooter = modalStatusEl.find('.modal-footer');
        const closeBtn = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
        const modalStatus = modalStatusEl.modal({
            keyboard: false,
            show: false,
            backdrop: 'static'
        });

        callAjax({url: '/--dev/module-manager/action', data: {action: _action}}, {
            beforeSend: function (jqXHR, settings) {
                modalFooter.html('');
                modalBody.html('<span class="text-info">Executing the command ...</span>')
                modalStatus.modal('show')
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalBody.html('<span class="text-danger">' + textStatus + '</span><p>' + errorThrown.message + '</p>');
                modalFooter.html(closeBtn)
            },
            success: function (data, textStatus, jqXHR) {
                $('#btnGroupDropClearSl').text('Clear...')
                topClearBtn.removeAttr('data-action')
                data = '<strong> The command has been executed: </strong><br/>' + data;
                modalBody.html('<div class="text-success" style="white-space: pre-line;">' + data + '</div>')
                modalFooter.html(closeBtn)
            }
        })
    }

    btnGroupDropClear.find('.dropdown-item').click(function (ev) {
        ev.preventDefault()
        const target = $(this)
        $('#btnGroupDropClearSl').text(target.text())
        topClearBtn.data('action', target.data('action'))
    })

    const btnDumpAutoload = $('#btn-dump-autoload')
    if (btnDumpAutoload)
        btnDumpAutoload.on('click', function (ev) {
            ev.preventDefault()
            cmdAction('autoload')
        })
    topClearBtn.on('click', function (ev) {
        ev.preventDefault()
        cmdAction(topClearBtn.data('action'))
    })

    // create module
    const formCreateModule = $('#formCreateModule')
    if (formCreateModule) {
        formCreateModule.submit(function (e) {
            e.preventDefault()
            const form = $(this);
            const url = form.attr('action');
            callAjax({url, data: form.serialize()}, {
                beforeSend: function (jqXHR, settings) {
                    formCreateModule.find('.alert').text('Executing the command ...');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    formCreateModule.find('.alert').text(textStatus + ': ' + jqXHR.responseText)
                },
                success: function (data, textStatus, jqXHR) {
                    formCreateModule.find('.alert').html('<pre><code>' + data + '</code><pre/>')
                    formCreateModule.find('.modal-create-btn').remove()
                    formCreateModule.find('.modal-close-btn').click(function (ev) {
                        window.location.reload()
                    })
                }
            })

            return false;
        })
    }

    // api doc
    const btnApidoc = $('#btn-dump-generate-api-doc')
    if(btnApidoc){
        btnApidoc.on('click', function (ev) {
            ev.preventDefault()
            cmdAction('api-generate')
        })
    }


})
function actionFunction(generator) {

    const getFromID = 'make_' + generator + '_form'
    const formGenerator = $('#' +getFromID)
    if(formGenerator){
        formGenerator.submit(function (e) {
            e.preventDefault()
            const form = $(this);
            const url = form.attr('action');
            callAjax({url, data: form.serialize()}, {
                beforeSend: function (jqXHR, settings) {
                    formGenerator.find('.alert').text('Executing the command ...');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    formGenerator.find('.alert').text(textStatus + ': ' + jqXHR.responseText)
                },
                success: function (data, textStatus, jqXHR) {
                    formGenerator.find('.alert').html(data)
                }
            })
        })
    }

}
