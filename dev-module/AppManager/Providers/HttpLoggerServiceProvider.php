<?php

namespace DevModule\AppManager\Providers;

use DevModule\AppManager\Middlewares\HttpLogger;
use Illuminate\Support\ServiceProvider;
use DevModule\AppManager\Code\LogProfile;
use DevModule\AppManager\Code\LogWriter;

class HttpLoggerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', HttpLogger::class);
        $this->app->singleton(LogProfile::class, config('app-manager.log_viewer.log_profile'));
        $this->app->singleton(LogWriter::class, config('app-manager.log_viewer.log_writer'));
    }

    public function register()
    {
    }
}
