<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: ApiDocGeneratorServiceProvider.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Providers/ApiDocGeneratorServiceProvider.php
 */

namespace DevModule\AppManager\Providers;

use Illuminate\Support\ServiceProvider;
use DevModule\AppManager\Commands\RebuildDocumentation;
use DevModule\AppManager\Commands\GenerateDocumentation;

class ApiDocGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

//        if ($this->app->runningInConsole()) {
            /*$this->commands([
                GenerateDocumentation::class,
                RebuildDocumentation::class,
            ]);*/
//        }
    }

    /**
     * Register the API doc commands.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->commands([
            GenerateDocumentation::class,
            RebuildDocumentation::class,
        ]);
    }
}
