<?php
Route::namespace('\\DevModule\AppManager\\Controllers')
    ->middleware('web')
    ->prefix('/--dev')
    ->as('--dev.')
    ->group(function () {
        Route::get('/', 'AppManagerController@index')->name('index');
        Route::get('module-manager', 'ModuleManagerController@index')->name('module.view');
        Route::post('module-manager', 'ModuleManagerController@createModule')->name('module.create');
        Route::post('module-manager/action', 'ModuleManagerController@runCmd')->name('module.action');
        Route::get('module-manager/detail/{name}', 'ModuleManagerController@detail')->name('module.detail');
        Route::get('module-manager/change/{name}/{action}', 'ModuleManagerController@change')
            ->where(['action' => '^(disable|enable)$'])
            ->name('module.change');

        Route::get('module-manager/delete/{name}', 'ModuleManagerController@delete')
            ->name('module.delete');
        Route::delete('module-manager/delete/{name}', 'ModuleManagerController@delete')
            ->name('module.ok-delete');

        Route::get('log-view', 'LogViewerController@index')->name('log.view');
//        Route::get('log-view2', 'LogViewerController@index')->name('log.view');
        Route::post('module-manager/generator', 'ModuleManagerController@generator')->name('module.generator');
    });

