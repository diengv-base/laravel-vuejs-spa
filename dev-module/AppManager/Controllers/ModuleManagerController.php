<?php


namespace DevModule\AppManager\Controllers;

use Artisan;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Str;
use Symfony\Component\Console\Output\BufferedOutput;

class ModuleManagerController extends DevController
{
    function index(Request $request)
    {
        $modules = \Module::all();
        return view('AppManager::module', compact('modules'));
    }

    private function formatOutput($output)
    {
        $output = str_replace('Created', '<span style="color: yellow">Created</span>', $output);
        return $output;
    }

    function toggleStatus(Request $request)
    {

    }

    function createModule(Request $request)
    {
        $this->validate($request, [
            'moduleName' => 'required'
        ]);

        $request->merge(["moduleName" => Str::studly($request->get('moduleName'))]);

        try {
            $output = new BufferedOutput;
            Artisan::call('module:make ' . $request->get('moduleName'). ' --api', [], $output);

            return response()->json($output->fetch());
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    function runCmd(Request $request)
    {
        $action = $request->get('action');
        $this->validate($request, ['action' => 'required']);
        try {
            $output = new BufferedOutput;
            switch ($action) {
                case 'autoload':
                    Artisan::call('clear-compiled', [], $output);
                    Artisan::call('optimize', [], $output);
                    Artisan::call('config:clear', [], $output);
                    break;
                case 'api-generate':
                    Artisan::call('apidoc:generate', [], $output);
                    break;
                default:
                    Artisan::call($action, [], $output);
                    break;
            }

            return response()->json($output->fetch());
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    function detail(Request $request, $name)
    {
        $module = \Module::find($name);
        $generators = ['command', 'migration', 'seed', 'controller', 'model', 'provider', 'middleware', 'mail',
            'notification', 'listener', 'request', 'event', 'job', 'provider', 'factory',
            'policy', 'rule', 'resource', 'test'];

        return view('AppManager::module-detail', compact('module', 'generators'));
    }

    function delete(Request $request, $name)
    {
        $module = \Module::find($name);
        if ($request->method() == 'GET') {
            return view('AppManager::module-delete', compact('module'));
        } else if ($request->method() == 'DELETE') {
            try {
                $module->delete();
                $filesystem = new Filesystem();
                $frontend = config('modules.paths.vue_module') . '/' . Str::kebab($module->getName());
                if ($filesystem->isDirectory($frontend))
                    $filesystem->deleteDirectory($frontend);

                $output = new BufferedOutput;
                Artisan::call('clear-compiled', [], $output);
                Artisan::call('optimize', [], $output);
                Artisan::call('config:clear', [], $output);

                return redirect()->route('--dev.module.view');
            } catch (\Exception $exception) {
                return redirect()->route('--dev.module.view')->withErrors(['message' => 'Can\'t delete module']);
            }

        }
    }

    function change(Request $request, $name, $action)
    {
        $module = \Module::find($name);
        $module->{$action}();

        return redirect()->route('--dev.module.detail', ['name' => $name]);
    }

    function generator(Request $request)
    {
        $forController = $request->get('controller_type');
        if ($forController && $forController != '') {
            $addCommand = '--' . $forController;
        } else {
            $addCommand = ' ';
        }
        $this->validate($request, [
            'generatorName' => 'required'
        ]);
        $request->merge(["generatorName" => Str::studly($request->get('generatorName'))]);
        try {
            $output = new BufferedOutput;
            Artisan::call('module:make-' . $request->get('generatorKey') . ' ' . $request->get('generatorName') . ' ' . $addCommand . ' ' . $request->get('name'), [], $output);
            return response()->json($output->fetch());
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

    }
}
