<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: DocumentationConfig.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Tools/DocumentationConfig.php
 */

namespace DevModule\AppManager\Tools;

class DocumentationConfig
{
    private $data;

    public function __construct(array $config = [])
    {
        $this->data = $config;
    }

    public function get($key, $default = null)
    {
        return data_get($this->data, $key, $default);
    }
}
