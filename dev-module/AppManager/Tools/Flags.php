<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: Flags.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Tools/Flags.php
 */

namespace DevModule\AppManager\Tools;

class Flags
{
    public static $shouldBeVerbose = false;
}
