<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: ResponseResolver.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Tools/ResponseResolver.php
 */

namespace DevModule\AppManager\Tools;

use Illuminate\Routing\Route;
use Symfony\Component\HttpFoundation\Response;
use DevModule\AppManager\Tools\ResponseStrategies\ResponseTagStrategy;
use DevModule\AppManager\Tools\ResponseStrategies\ResponseCallStrategy;
use DevModule\AppManager\Tools\ResponseStrategies\ResponseFileStrategy;
use DevModule\AppManager\Tools\ResponseStrategies\TransformerTagsStrategy;

class ResponseResolver
{
    /**
     * @var array
     */
    public static $strategies = [
        ResponseTagStrategy::class,
        TransformerTagsStrategy::class,
        ResponseFileStrategy::class,
        ResponseCallStrategy::class,
    ];

    /**
     * @var Route
     */
    private $route;

    /**
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * @param array $tags
     * @param array $routeProps
     *
     * @return array|null
     */
    private function resolve(array $tags, array $routeProps)
    {
        foreach (static::$strategies as $strategy) {
            $strategy = new $strategy();

            /** @var Response[]|null $response */
            $responses = $strategy($this->route, $tags, $routeProps);

            if (! is_null($responses)) {
                return array_map(function (Response $response) {
                    return ['status' => $response->getStatusCode(), 'content' => $this->getResponseContent($response)];
                }, $responses);
            }
        }
    }

    /**
     * @param $route
     * @param $tags
     * @param $routeProps
     *
     * @return array
     */
    public static function getResponse($route, $tags, $routeProps)
    {
        return (new static($route))->resolve($tags, $routeProps);
    }

    /**
     * @param $response
     *
     * @return mixed
     */
    private function getResponseContent($response)
    {
        return $response ? $response->getContent() : '';
    }
}
