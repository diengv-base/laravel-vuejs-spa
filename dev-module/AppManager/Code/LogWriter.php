<?php

namespace DevModule\AppManager\Code;

use Illuminate\Http\Request;

interface LogWriter
{
    public function logRequest(Request $request);
}
