<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: SinceTag.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Code/Reflection/DocBlock/Tag/SinceTag.php
 */

namespace DevModule\AppManager\Reflection\DocBlock\Tag;

use DevModule\AppManager\Reflection\DocBlock\Tag\VersionTag;

class SinceTag extends VersionTag
{
}
