<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: DeprecatedTag.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Code/Reflection/DocBlock/Tag/DeprecatedTag.php
 */

namespace DevModule\AppManager\Reflection\DocBlock\Tag;

use DevModule\AppManager\Reflection\DocBlock\Tag\VersionTag;

class DeprecatedTag extends VersionTag
{
}
