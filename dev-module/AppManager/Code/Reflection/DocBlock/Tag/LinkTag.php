<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: LinkTag.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Code/Reflection/DocBlock/Tag/LinkTag.php
 */

namespace DevModule\AppManager\Reflection\DocBlock\Tag;

use DevModule\AppManager\Reflection\DocBlock\Tag;

class LinkTag extends Tag
{
    /** @var string */
    protected $link = '';

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = "{$this->link} {$this->description}";
        }

        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        parent::setContent($content);
        $parts = preg_split('/\s+/Su', $this->description, 2);

        $this->link = $parts[0];

        $this->setDescription(isset($parts[1]) ? $parts[1] : $parts[0]);

        $this->content = $content;
        return $this;
    }

    /**
    * Gets the link
    *
    * @return string
    */
    public function getLink()
    {
        return $this->link;
    }

    /**
    * Sets the link
    *
    * @param string $link The link
    *
    * @return $this
    */
    public function setLink($link)
    {
        $this->link = $link;

        $this->content = null;
        return $this;
    }
}
