<?php
/**
 * Copyright (c) 2019.
 * Project: laravel-vuejs-spa
 * File: SeeTag.php
 * Path: D:/projects/laravel-vuejs-spa/dev-module/AppManager/Code/Reflection/DocBlock/Tag/SeeTag.php
 */

namespace DevModule\AppManager\Reflection\DocBlock\Tag;

use DevModule\AppManager\Reflection\DocBlock\Tag;

class SeeTag extends Tag
{
    /** @var string */
    protected $refers = null;

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = "{$this->refers} {$this->description}";
        }
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        parent::setContent($content);
        $parts = preg_split('/\s+/Su', $this->description, 2);

        // any output is considered a type
        $this->refers = $parts[0];

        $this->setDescription(isset($parts[1]) ? $parts[1] : '');

        $this->content = $content;
        return $this;
    }

    /**
     * Gets the structural element this tag refers to.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->refers;
    }

    /**
     * Sets the structural element this tag refers to.
     * 
     * @param string $refers The new type this tag refers to.
     * 
     * @return $this
     */
    public function setReference($refers)
    {
        $this->refers = $refers;

        $this->content = null;
        return $this;
    }
}
