<?php

namespace DevModule\AppManager\Code;

use Illuminate\Http\Request;

class LogNonGetRequests implements LogProfile
{
    public function shouldLogRequest(Request $request): bool
    {
        if ($request->ajax()) {
            return in_array(strtolower($request->method()), ['get', 'post', 'put', 'patch', 'delete']);
        }

        return in_array(strtolower($request->method()), ['post', 'put', 'patch', 'delete']);
    }
}
