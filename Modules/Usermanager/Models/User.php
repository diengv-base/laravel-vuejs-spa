<?php

namespace Modules\Usermanager\Models;

use http\Env\Response;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUser(){
        $data = self::all();
        return $data;
    }

    public function getUserById($id){
        $user = self::find($id);
        try{
          return $user;
        }catch (Exception $e){
            return $e;
        }
    }

    public function deleteUser($id){
        $result = self::findOrFail($id)->delete();
        return $result;
    }
}
