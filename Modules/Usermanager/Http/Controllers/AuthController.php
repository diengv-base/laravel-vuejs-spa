<?php

namespace Modules\Usermanager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Usermanager\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\BaseController;

class AuthController extends BaseController
{

    /*
     * Authentication
     */

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $userdata = array(
            "email" => $request->email,
            "password" => $request->password,
        );
        if (Auth::attempt($userdata)) {
            $request->session()->regenerate();
            return \response()->json('success', 200);
        } else {
            $errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
            return \response()->json($errors, 200);
        }
    }

}
