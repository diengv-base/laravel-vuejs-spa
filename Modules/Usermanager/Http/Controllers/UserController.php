<?php

namespace Modules\Usermanager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Usermanager\Models\User;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    public function list()
    {
        $user = new User();
        $data = $user->getUser();
        return \response()->json($data, 200);
    }

    /*
     * Function update user
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email',
        ]);
        $user = new User();
        $data = $user->getUserById($id);
        if ($data && $data != '') {
            $data->name = $request->name;
            $data->email = $request->email;
            $data->save();
            return $this->responseAPI(true, $data, 200);
        } else {
            return $this->responseAPI(false, 'User Not Found', 200);
        }
    }
    /*
     * Function delete user
     */
    public function delete($id){
        $user = new User();
        $user->deleteUser($id);
        return $this->responseAPI(true, 'Delete Success', 200);
    }

}
